# Sentiment Analysis
---

### Requirements
--- 
- pandas
- matplotlib
- konlpy
- numpy
- jupyter notebook
- tensorflow == 2.0
- imbalanced-learn
- keras-rectified-adam (radm optimzation)
- pytorch >= 1.1
- torchvision
- torch-pretrained-bert
- seaborn
- wordcloud
- gensim(word2vec library)
- xgboost

### Directory & File structure.
---
```
```

추후 계속 추가 예정.



License MIT © Nathan